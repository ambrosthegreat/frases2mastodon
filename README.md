# Frases

___02/04/2020___
Bot para Mastodon que lee un fichero de texto con frases o palabras y ejecuta dos tareas:

	+ Publicar periódicamente una frase o palabra
	+ Responder con una frase o palabra cuando se le cita

Desarrollador por https://hispatodon.club/@AmbrosTheGreat

Puedes seguir el bot en esta cuenta: https://botsin.space/@insultos

El script está hecho específicamente para correr en Heroku.

Este bot está basado en la idea de Cheap Bots, Toot Sweet! <https://cheapbotstootsweet.com>.

___03/04/2020___
Ahora la respuesta incluye también a todos los citados en el mensaje original.
Implementado sistema de comandos; las menciones como método para respuesta del bot podían provocar el caos si dos bots se empiezan a citar entre ellos.

___04/04/2020___
El bot ya sólo responde a solicitudes de humanos.

___11/07/2020___
El bot estaba metido dentro de una app con todos mis bots para correr sólo una tarea en el servidor para todos.
Finalmente, he vuelto a separar el bot.
