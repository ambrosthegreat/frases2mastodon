#!/usr/bin/env python3
#
# frases
# Librería para extraer una frase de un fichero de texto
#
# Documentación librería Mastodon: https://mastodonpy.readthedocs.io/en/stable/
#

# LIBRERIAS
from random import randint
from chistes import buscar_chiste

def buscar_en_fich( nom_frases, cabecera = '' ):
    # Buscar frase
    frases = []
    f_frases = open( nom_frases, 'r' )
    for linea in f_frases:
        linea = linea.replace( '\\n', '\n' )
        frases.append( linea )
    f_frases.close()
    id_frase = randint( 1, len( frases ) - 1 )
    frase = frases[ id_frase ]
    frase = frase.strip()
    if cabecera != '':
        frase = cabecera + '\n' + frase.strip()
    return frase

def _buscar_chiste( cabecera ):
    if cabecera != '':
        cabecera = cabecera + '\n'
    # salida = cabecera + buscar_chiste( cabecera ).strip()
    salida = buscar_chiste( cabecera ).strip()
    return salida

def buscar_frase( nom_frases = '', cabecera = '' ):
    if nom_frases:
        salida = buscar_en_fich( nom_frases, cabecera )
    else:
        salida = _buscar_chiste( cabecera )
    return salida

def ayuda( masto, notificacion ):
    mensaje = '@' + masto.solicitante_notif( notificacion ) +'\n'
    mensaje = mensaje + 'Bot de proceso de mensajes\n---\n'
    mensaje = mensaje + 'El bot procesa un comando dentro del mensaje con las siguientes opciones:\n'
    mensaje = mensaje + '<h> Esta ayuda\n'
    mensaje = mensaje + '<r> Devolver una frase\n'
    mensaje = mensaje + '\nNo se procesan solicitudes de bots\n'
    masto.toot_texto( mensaje, masto.id_notif( notificacion ), masto.visibilidad_notif( notificacion ) )

def run_frase( masto, notificacion, fich_frases='' ):
    solicitante = "@" + masto.solicitante_notif( notificacion )
    mi_frase = buscar_frase( fich_frases, solicitante )

    if mi_frase != '':
        respuesta = mi_frase
        respuesta = masto.citados( notificacion, respuesta )
        if len( respuesta ) >= masto.masto_max():
            respuesta = "Ha habido un error " + solicitante + "\nPor favor, vuelve a intentarlo."
        masto.toot_texto( respuesta, masto.id_notif( notificacion), masto.visibilidad_notif( notificacion ) )

def comandos( masto, notificacion, fich_frases='' ):
    # Comandos: h, r
    minus = masto.cuerpo_notif( notificacion ).lower()
    if minus.find( '<h>' )>0:
        ayuda( masto, notificacion )
    elif minus.find( '<r>' )>0:
        run_frase( masto, notificacion, fich_frases )
    return
