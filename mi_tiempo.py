#!/usr/bin/env python3
#
# mi_tiempo
# Librería con mis funciones de control de tiempo
#
# Documentación librería Mastodon: https://mastodonpy.readthedocs.io/en/stable/
#

# LIBRERIAS
from datetime import datetime, timedelta
from dateutil import tz

# FUNCIONES
def mi_hora():
    zona = tz.gettz( 'Europe/Madrid' )
    hora_servidor = datetime.now()
    hora = hora_servidor.astimezone( zona )
    return hora

def a_zona( fecha ):
    zona = tz.gettz( 'Europe/Madrid' )
    salida = fecha.astimezone( zona )
    return salida

def toca_alarma( alarma ):
    hora = mi_hora()
    salida = ( hora > alarma )
    return salida

def sumar_horas( tiempo, horas ):
    return tiempo + timedelta( hours = horas )
