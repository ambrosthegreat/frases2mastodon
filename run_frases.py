#!/usr/bin/env python3
#
# run_frases
# Lanza una frase al azar para cada uno de los bots que hay funcionando
# Rajoy, insultos y Chuck Norris toman las frases de un fichero de texto
# Chistes hace webscraping de varias webs
#

# CONSTANTES
F_CHUCK = 'chuck_norris.txt' # Fichero de frases de Chuck Norris
F_INSULTOS = 'insultos.txt' # Fichero de insultos
F_RAJOY = 'rajoy.txt' # Fichero de frases de Rajoy

INST_URL = 'https://botsin.space/' # URL de la instancia para los bots

T_ESPERA = 5        # Tiempo de espera entre comprobación de eventos
T_RECUPERA = 30     # Tiempo de recuperación en segundos después de un error

TA_INSULTOS = 1     # Horas hasta siguiente frase de insultos
TA_RAJOY = 6        # Horas hasta siguiente frase de Rajoy
TA_CHUCK = 3        # Horas hasta siguiente frase de Chuck Norris
TA_CHISTES = 1      # Horas hasta siguiente chiste

# LIBRERIAS
from argparse import ArgumentParser
from frases import buscar_frase, comandos
from mi_masto import Mi_masto
from mi_tiempo import mi_hora, toca_alarma, sumar_horas
from os import getcwd
# from sys import exc_info as error
from time import sleep

# FUNCIONES
def _args():
    #Argumentos
    parser = ArgumentParser( description='Publica frases de un fichero de texto' )
    parser.add_argument( '-ab', '--app_token_chistes', help='Token de la app para chistes', type=str )
    parser.add_argument( '-ac', '--app_token_chuck', help='Token de la app para Chuck Norris', type=str )
    parser.add_argument( '-ai', '--app_token_insultos', help='Token de la app para insultos', type=str )
    parser.add_argument( '-ar', '--app_token_rajoy', help='Token de la app para Rajoy', type=str )
    return parser.parse_args()

def _ruta_entera( nom_fich ):
    return getcwd() + '/ficheros_frases/' + nom_fich

def _preparar_bot( app_token, horas, nom_fich = '' ):
    masto = Mi_masto( INST_URL, app_token )
    if nom_fich != '':
        fic_salida = _ruta_entera( nom_fich )
    else:
        fic_salida = None
    alarma = sumar_horas( mi_hora(), horas )
    return masto, fic_salida, alarma

def _trata_alarma( mastodon, alarma, horas, nom_fich = '' ):
    salida = alarma

    if toca_alarma( alarma ):
        mi_frase = buscar_frase( nom_fich )
        if mi_frase != '':
            mastodon.toot_texto( buscar_frase( nom_fich ) )
            salida = sumar_horas( alarma, horas )
    return salida

def _trata_notifs( mastodon, t_espera, nom_fich='' ):
    notifs = mastodon.comprueba_notif( t_espera )
    for notif in notifs:
        comandos( mastodon, notif, nom_fich )

def _tratar( mastodon, alarma, horas, t_espera, nom_fich='' ):
    salida = _trata_alarma( mastodon, alarma, horas, nom_fich ) 
    _trata_notifs( mastodon, t_espera, nom_fich )
    return salida

# MAIN
args = _args()
hora = mi_hora()

m_insultos, f_insultos, a_insultos = _preparar_bot( args.app_token_insultos, TA_INSULTOS, F_INSULTOS )
m_chuck, f_chuck, a_chuck = _preparar_bot( args.app_token_chuck, TA_CHUCK, F_CHUCK )
m_rajoy, f_rajoy, a_rajoy = _preparar_bot( args.app_token_rajoy, TA_RAJOY, F_RAJOY )
m_chistes, f_chistes, a_chistes = _preparar_bot( args.app_token_chistes, TA_CHISTES )

while True:
    a_insultos = _tratar( m_insultos, a_insultos, TA_INSULTOS, T_ESPERA, nom_fich=f_insultos )
    a_rajoy = _tratar( m_rajoy, a_rajoy, TA_RAJOY, T_ESPERA, nom_fich=f_rajoy )
    a_chuck = _tratar( m_chuck, a_chuck, TA_CHUCK, T_ESPERA, nom_fich=f_chuck )

    a_chistes = _tratar( m_chistes, a_chistes, TA_CHISTES, T_ESPERA )

    sleep( T_ESPERA )
